# Pecas Reader

[es] Lee archivos HTML como ebooks, pero con una mayor simplicidad y flexibilidad.

[en] Read HTML files as ebooks, but with more simplicity and flexibility.

## Tree

```
.
├── 1. exe
├── 2. cdnjs
├── 3. dist
├── 4. font-families
├── 5. tests
└── 6. themes
```

[es] Descripción:

1. Binarios para realizar operaciones en el repo
2. Ficheros para CDNJS.com
3. JavaScript para distribución
4. Colección de familia de fuentes
5. Ficheros para pruebas
6. Temas

[es] Description:

1. Binaries for repo operations
2. Files for CDNJS.com
3. JavaScript for distribution
4. Font families collection
5. Files for tests
6. Themes

## Themes

[es] Con Pecas Reader puedes seleccionar un tema para la familia de fuentes, la navegación y el diseño de tu ebook.
¡Envía un pull request con tu tema!

[en] With Pecas Reader you can select a theme for the font families, the design and the navigation of your ebook.
Send a pull request with your theme!

### Font families

[es] Para permitir mayores libertades a los lectores, las fuentes se gestionan de manera independiente a las plantillas.
Su estructura es:

```
Nombre a mostrar en Pecas Reader
Familia de fuentes como se declara en font-family
```

[en] In order to give more freedoms to the readers, the fonts are managed independently from the templates.
Its structure is:

```
Display name in Pecas Reader
Font family as declared in font-family
```

### Navs

[es] Los temas de navegación permiten control al lector sobre el ebook. Cada navegación es uno o más archivos JavaScript
contenidos en un directorio más de un archivo `name.txt` en donde se indica el nombre a mostrar en Pecas Reader.

[en] The navs themes allow more control for the reader over the ebook. Each nav is one or more JavaScript files contained
in a directory plus a `name.txt` file that indicates the display name in Pecas Reader.

### Templates

[es] Las plantillas son temas que le dan diseño al ebook. Su organización es similar a los navs, pero en lugar de archivos
JavaScript, son archivos CSS.

[en] The templates are themes that gives design to the ebook. Its organization is similar to navs, but instead of JavaScript files,
they are CSS files.
