# A la mierda los libros, si y solo si…

Siempre intento ser muy claro acerca de algo: los libros en nuestros
días, por sí mismos, solo son un producto de sobra. Sí, hemos
construido una industria para hacerlos. Pero ¿tienes la capacidad
de publicar con tus propios medios?

Lo más probable es que no. Casi seguro es que te falta algo:
no son tuyas las máquinas que según se requieren; no cuentas
con las habilidades; no tienes el conocimiento o no posees los
contactos. Nada te pertenece.

Hemos alcanzado la capacidad de producción para publicar en un
par de horas lo que en el pasado tomó siglos. Eso es sorprendente…
y espeluznante. ¿Qué estamos publicando ahora? *¿Por qué estamos
produciendo tanto?* Nuestra capacidad de lectura no ha mejorado
al mismo ritmo ---quizá hemos estado perdiendo esa habilidad---.

Ahora somos más personas, pero es una suposición contemporánea
que cada persona requiere un ejemplar. Tenemos librerías públicas.
Estas solían ser una gran idea. Ahora es de los pocos espacios
donde las personas pueden ir y disfrutar sin tener que pagar
un centavo. El último bastión de un mundo antes de su monetización
global. Y algunas veces ni siquiera eso, debido a que están detrás
de un muro: pago de suscripciones o credenciales universitarias;
o porque la mayoría de nosotros preferimos las cafeterías, las
bibliotecas públicas son para personas pobres, raras o viejas,
¿cierto?

Y nos la pasamos alabando los libros cual si fueran sagrados
productos de nuestra cultura. Aunque en realidad lo que hacemos
es apoyar un bien de consumo. Tú no los haces, tú no los lees:
solo los compras y los pones en un librero. Tú no eres su propietario
ni siquiera miras lo que hay adentro: solo compras y los dejas
en tu cuenta de Amazon. Eres un consumidor y eso hace pensarte
a ti mismo como alguien que apoya a nuestra cultura.

Como editores hacemos del libro el centro de todo: ferias, talleres,
reuniones, grados universitarios y publicidad. Como editores
queremos venderte el siguiente *best-seller*, el formato de libro
más reciente: «el futuro de la lectura». Aunque en realidad lo
que queremos es tu dinero. Nosotros sabemos que no lees. Nosotros
estamos al tanto de que no quieres libros que van a explotar
la burbuja en la que vives. Nosotros tenemos conocimiento de
que solo quieres entretenerte. Nosotros entendemos que ansías
decir cuántos libros has «leído». Solo queremos que nos estés
comprando y comprando. No nos importas.

Antes de que pasara toda esta mierda en la edición, los libros
eran un producto raro y de difícil producción. A partir de ellos
podíamos ver la complejidad de nuestro mundo: sus medios de producción,
su estructura y sus conflictos. Editores fueron asesinados porque
quisieron ofrecerte algo muy importante que leer. Ahora los editores
son premiados con viajes, apoyos económicos o cenas pomposas.
La mayoría de los editores dejaron de ser una amenaza. En su
lugar, son los gerentes del debate público; es decir, lo que
puedes decir, pensar o sentir.

En nuestros días la mayoría de los libros solo muestran cómo
los pilares de nuestro mundo han sido desplazados como otro bien
a disposición del mercado. Vemos el papel, vemos la tinta, vemos
las fuentes y vemos el código. Y después de esta primera mirada,
empezamos a darnos cuenta que nuestros libros son principalmente
un engrane de la maquinaria del consumo global.

Solo hasta este punto de manera clara podemos observar la cadena
de explotación necesaria para alcanzar este tipo de productividad.
*¿Quién o qué se beneficia de ello?* Los autores ya no pueden
vivir de eso. Las personas involucradas en la producción de libros
---impresores, correctores de pruebas, diseñadores, editores
y más--- con trabajos ganan un salario mínimo. Muchos árboles
y recursos han sido utilizados para obtener ganancias.

De nuevo, hemos alcanzado la capacidad de producción para publicar
en un par de horas lo que en el pasado tomó siglos, *¿dónde quedó
toda esa riqueza?* En nuestros bolsillos no, siempre hemos tenido
que pagar para poder producir libros o tenerlos.

Entonces, ¿qué hace que quieras los libros que tal vez nunca
leerás? Si y solo si la edición es lo que ahora es y no queremos
hacer nada para cambiarlo, bueno: a la mierda los libros.
