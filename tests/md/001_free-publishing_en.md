# From Publishing with Free Software to Free Publishing

This blog is about “free publishing” but, what does that mean?
The term “free” isn\'t only problematic in English. Maybe more
in other languages because of the confusion between “free as
in beer” and “free as in speech.” But by itself the concept of
freedom is so ambiguous than even in Philosophy we are very careful
in its use. Even though it is a problem, I like that the term
doesn\'t have a clear definition---in the end, how free could
we be if freedom is well defined?

Some years ago, when I started to work hand-in-hand with Programando
Libreros and Hacklib, I realized that we weren\'t just doing
publishing with free software. We are doing free publishing.
So I attempted to define it in a post but it doesn\'t convince
me anymore.

The term was floating around until December, 2018. At Contracorriente---yearly
fanzine fair celebrated in Xalapa, Mexico---Hacklib and I were
invited to give a talk about publishing and free software. Between
all of us we made a poster of everything we talked about that
day.

![Poster made at Contracorriente, nice, isn\'t it?](../img/p001_i001.jpg)

The poster was very helpful because in a simple Venn diagram
we were able to distinguish several intersections of activities
that involve our work.

So I\'m not gonna define publishing, free software or politics---it
is my fucking blog so I can write whatever I want xD and you
can duckduckgo it without a satisfactory answer. As you can see,
there are at least two very familiar intersections: cultural
policies and hacktivism. I dunno how it is in your country, but
in Mexico we have very strong cultural policies for publishing---or
at least that is what publishers *think* and are comfortable
with it, no matter that most of the time they go against open
access and readers rights.

“Hacktivism” is a fuzzy term, but it could be clear if we realized
that code as property is not the only way we can define it. Actually
it is very problematic because property isn\'t a natural right,
but one that is produced by our societies and protected by our
states---yeah, individuality isn\'t the foundation of rights
and laws, but a construction of the self produced society. So,
do I have to mention that property rights isn\'t as fair as we
would like?

![Venn diagram of publishing, free software and politics.](../img/p001_i002_en.png)

Between publishing and free software we get “publishing with
free software.” What does that imply? It is the act of publishing
using software that accomplishes the famous---infamous?---[four
freedoms](https://en.wikipedia.org/wiki/The_Free_Software_Definition).
For people that use software as a tool, this means that, first,
we aren\'t forced to pay anything in order to use software. Second,
we have access to the code and do whatever we want with it. Third---and
for me the most important---we can be part of a community, instead
of treated as a consumer.

It sounds great, doesn\'t it? But we have a little problem: the
freedom only applies to software. As a publisher you can benefit
from free software and that doesn\'t mean you have to free your
work. Penguin Random House---the Google of publishing---one day
could decide to use TeX or Pandoc, saving tons of money and at
the same time keep the monopoly of publishing.

Stallman saw the problem with manuals published by O\'Reilly
and he proposed the +++GNU+++ Free Documentation License. But
by doing so he trickly distinguished different kinds of works.
It is interesting to see texts as functional works, matter of
opinion or aesthetics but in the publishing industry nobody gives
a fuck about that. The distinctions work great between writers
and readers, but it doesn\'t problematize the fact that publishers
are the ones who decide the path of almost all of our text-centered
culture.

In my opinion, that\'s dangerous at least. So I prefer another
tricky distinction. Big publishers and their mimetic branch---the
so called “indie” publishing---only cares about two things: sales
and reputation. They want to live *well* and get social recognition
from the *good* books they publish. If one day software communities
develop some desktop publishing or typesetting easy-to-use and
suitable for all their *professional* needs, we would see how
“suddenly” the publishing industry embraces free software.

So, why don\'t we distinguish published works by their funding
and sense of community? If what you publish has public funding---for
your knowledge, in Mexico practically all publishing has this
kind of funding---it would be fair to release the files and leave
hard copies for sale: we already paid for that. This is a very
common argument among supporters of open access in science, but
we can go beyond that. No matter if the work relies on functionality,
matter of opinion or aesthetics; whether its a scientific paper,
a philosophy essay or a novel and it has public funding, we have
already paid for access, come on!

You can still sell publications and go to Messe Frankfurt, Guadalajara
International Book Fair or Beijing Book Fair: it is just doing
business with the *bare minium* of social and political awareness.
Why do you want more money from us if we\'ve already given it
to you?---and you receive almost all of the profits, leaving
the authors with just the satisfaction of seeing her work published…

The sense of community goes here. In a world where one of the
main problems is artificial scarcity---paywalls instead of actual
walls---we need to apply copyleft or, even better, copyfarleft
licenses in our published works. They aren\'t the solution, but
they are a support to maintain the freedom and the access in
publishing.

As it goes, we need free tools but also free works. I already
have the tools but lack the permission to publish some books
that I really like. I don\'t want that happen to you with my
work. So we need a publishing ecosystem where we have access
to all files of a particular edition---our “source code” and
“binary files”---and also to the tools---the free software---so
we can improve, as a community, the quality and the access of
works and its required skills. Who doesn\'t want that?

With these political strains, free software tools and publishing
as a way of living as a publisher, writer and reader, free publishing
is a pathway. With Programando Libreros and Hacklib we use free
software, we invest time in activism and we work in publishing:
*we do free publishing, what about you?*
