/*
  TODO:
    1. Generar DOM
    2. Añadir metas
    3. Añadir TOC
    4. Emular varias páginas
*/

// Inserts CSS
function addCSS () {
  let style = document.createElement('style');
  style.innerHTML = css;
  document.body.appendChild(style)
}

// TODO: refactorizar
// Increases or decreases font size
function zoom (up) {
  var section = document.getElementsByTagName('section')[0],
      childs  = section.getElementsByTagName('*');

  for (var i = 0; i < childs.length; i++) {
    var e         = childs[i],
        font_size = parseFloat(window.getComputedStyle(e, null)
                                     .getPropertyValue('font-size'));

    if (font_size >= 1) {
      font_size = up ? font_size + 1 : font_size - 1;
      e.style.fontSize = font_size + 'px';
    }
  }
}

// TODO: refactorizar
// Changes view mode
function mode (e) {
  var label       = e.innerHTML,
      el_section  = document.getElementsByTagName('section')[0].getElementsByTagName('*'),
      el_footer   = document.getElementsByTagName('footer')[0].getElementsByTagName('*');

  function change_color (collection, color) {
    for (var i = 0; i < collection.length; i++) {
      var e     = collection[i];

      if (e.nodeName == 'H1' || e.nodeName == 'H2' || e.nodeName == 'H3' ||
          e.nodeName == 'H4' || e.nodeName == 'H5' || e.nodeName == 'H6' ||
          e.nodeName == 'P' || e.nodeName == 'SPAN' ||
          e.nodeName == 'FIGCAPTION') {

        if (!e.parentNode.classList.contains('hashover-content') &&
            !e.parentNode.classList.contains('hashover-form-links') &&
            !e.classList.contains('hashover-form-links') &&
            !e.parentNode.parentNode.classList.contains('hashover-formatting-table')) {
          e.style.color = color;
        }
      } 
    }
  }

  if (label == 'N') {
    e.innerHTML = 'D';
    document.body.classList.add('black');
    change_color(el_section, 'white');
    change_color(el_footer, 'white');
  } else {
    e.innerHTML = 'N';
    document.body.classList.remove('black');
    change_color(el_section, 'inherit');
    change_color(el_footer, 'inherit');
  }
}

// Inyecta elementos al DOM
document.onreadystatechange = () => {
  if (document.readyState === 'complete') {
    addCSS();
  }
};

